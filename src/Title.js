import React, {useEffect, useState} from 'react';
import {store} from "./Store";

export const Title = () => {
    const [title, setTitle] = useState()

    useEffect(() => {
        const subscription = store.subscribe(state => {
            setTitle(state.title)
        });

        return () => subscription.unSubscribe()
    }, [])

    return (<div>{title}</div>)
}