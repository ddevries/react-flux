import React, {useEffect, useState} from 'react';
import './App.css';
import {Title} from "./Title";
import {Input} from "./Input";
import {store} from "./Store";


function App() {
    const [shouldDisplayTitle, setShouldDisplayTitle] = useState(false)
    useEffect(()=> {
        const subscription = store.subscribe((state) => {
            setShouldDisplayTitle(state.shouldShowTitle)
        })
        return () => subscription.unSubscribe()
    }, [])

    return (
    <div className="App">
        {shouldDisplayTitle && <Title />}
      <Input />

      <button onClick={() => store.dispatch({type: 'toggle_shouldShowTitle'})}>{shouldDisplayTitle ? 'hide title' : 'show title'}</button>
    </div>
    );
}

export default App;
