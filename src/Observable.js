export const createSubject = () => {
    const _observables = []

    const subscribe = (observable) => {
        _observables.push(observable)
        return {
            unSubscribe: () => {
                _observables.splice(_observables.indexOf(observable),1)
            }
        }
    }

    const notify = (value) => {
        _observables.forEach(observable => observable.update(value))
    }

    return {
        subscribe,
        notify
    }
}


export const createObservable = (initialValue) => {
    const _listeners = []
    let _value = initialValue;

    const subscribe = (listener) => {
        _listeners.push(listener)
        listener(_value)
        return {
            unSubscribe: () => {
                _listeners.splice(_listeners.indexOf(listener),1)
            }
        }
    }

    return {
        subscribe,
        get value() {
            if(_value === null) {
                return _value
            }

            if(typeof _value == 'object') {
                return Array.isArray(_value) ?
                    [..._value] : {..._value}
            }

            return _value
        },
        set value(value) {
            _value = value;
            _listeners.forEach(callback => callback(this.value))
        }
    }
}