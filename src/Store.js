import {createStore} from "./Flux";

const initialState = {
    title:  'initial state',
    shouldShowTitle: true
}

const reducer = (state, action) => {
    switch (action.type){
        case 'set_title':
            state.title = action.payload
            break;
        case 'toggle_shouldShowTitle':
            state.shouldShowTitle = !state.shouldShowTitle
            break;
    }

    return state;
}

export const store = createStore(reducer, initialState)
