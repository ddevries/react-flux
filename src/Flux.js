import {createObservable} from "./Observable";

export const createStore = (reducer, initialState) => {
    const _state = createObservable(initialState)
    const subscribe = _state.subscribe
    const dispatch = (action) => {
        _state.value = reducer(_state.value, action)
    }

    return {
        dispatch,
        subscribe,
    }
}