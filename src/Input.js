import React, {useEffect, useState} from 'react';
import {store} from "./Store";

export const Input = () => {
    const [value, setValue] = useState()

    useEffect(() => {
        const subscription = store.subscribe(state => {
            setValue(state.title)
        });
        return () => subscription.unSubscribe()
    }, [])

    return (
        <input value={value} onChange={(event) => {
            store.dispatch({
                type: 'set_title',
                payload: event.target.value
            })
        }} />
    )
}